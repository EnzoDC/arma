ASMDIR=asm
DIGCOMPDIR=digcomp
DIGLOG=../dig/bin/diglog

.PHONY: clean all run

all:
	for filename in $(ASMDIR)/*.asm ; do echo "\n compiling $${filename}"; $(DIGCOMPDIR)/digcomp "$${filename}" ; done


clean:
	rm $(ASMDIR)/*.lo $(ASMDIR)/*.hi

run:
	 $(DIGLOG) digproc/*.lgf &