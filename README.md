This archive contains :
- a pdf file RAPPORt.pdf that answers the homework
- a folder *digcomp* with the sources of the **modified** digcomp program
- a digproc folder which contains the lgf  
- an asm folder with the sources of the tests
- a barebone makefile that helped me streamline the process of compiling, cleaning files and running diglog
- this very [README.md](README.md)
