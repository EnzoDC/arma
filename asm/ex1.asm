# --- ex1.asm ---
# test		: ldi, adi, jmp
# result	: at PC=1, we have r1 = 42. 
# 		      at PC=2, we have r0 = 59 as expected 
#			  (by looking directly into the register)
	ldi r1, 42
	addi r0, r1, 17
end:	
	jmp end
