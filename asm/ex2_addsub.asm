# --- ex2_addsub.asm ---
# test		: add, sub
# result	: at PC=3 we have r0 = 83
#			  at PC=6 we have r1 = 1
	ldi r1, 42
	ldi r2, 41
	add r0, r1, r2
	ldi r1, 42
	ldi r2, 41
	sub r0, r1, r2
end:	
	jmp end
