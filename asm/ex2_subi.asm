# --- ex2_subi.asm ---
# test		: subi
# result	: at PC=3 we have r0 = 25
	ldi r1, 42
	subi r0, r1, 17
end:	
	jmp end
