# --- ex3_ioj.asm ---
# test		: in, out and jeq
# result	: when we input chars on the keyboard, they appear onscreen
#			  when we press Enter, the program stops.
nop
	ldi r1, 13
loop:
	in r0
	out r0
	jeq r0, r1, end
	jmp loop
end:
	jmp end
