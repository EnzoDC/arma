# --- ex4_add10.asm ---
# test      : addi, jeq
# result    : we see '<' outputted on screen, as expected.
# multiply (r0) by 10
    nop
# setting r0 for testing purpose
    mov r0, 6
    mov r1, 0       # index
    mov r2, 0       # res
# goto end if r0=0
    jeq r0, r1, end
loop:
    addi r2, r2, 10
    addi r1, r1, 1
    jlt r1, r0, loop
end:
    out r2          # we should see '<'
    jmp end         # force to end
