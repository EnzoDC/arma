# --- ex4_outcaps.asm ---
# test      : ld, st, in, jeq
# result    : when we input "qwq", we get "QWQ" outputted, as expected
# take userinput and prints it in caps once Enter is pressed
# init
    nop
    ldi r0, 255     # cst for jeq purposes (keyboard input)
    ldi r4, 13      # cst for jeq purposes (Enter input)
    ldi r5, 97      # cst for jle purposes (ascii code for 'a')
    ldi r6, 122     # cst for jle purposes (ascii code for 'z')
    ldi r1, 0       # pointer j to the beginning of the inputs in Addr
    ldi r2, 0       # index i
input:
    in r3           # char of user
# block until we get a char different than empty input
    jeq r0, r3, input
# check if Enter was pressed
    jeq r3, r4, print
# store value in mem, and incr
    mov (r2), r3    # r3 --> MEM[i]
    addi r2, r2, 1  # i++
    jmp input
print:
# check if we are done printing
    jeq r1, r2, end
    mov r3, (r1)    # r3 <-- MEM[j]
    addi r1, r1, 1  # j++
# check if char is lower case 
    jle r5, r3, caps
    out r3
    jmp print
caps:
    subi r3, r3, 16 # caps letter ar just 32 bits under the lower-case letters in ascii
    nop
    subi r3, r3, 16 # we substract it two times because we cant sub more than 31.
    out r3
    jmp print
end:
    jmp end         # End of program. loops on itself to avoid reading garbage data
