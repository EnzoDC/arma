# --- ex5_div.asm ---
# test      : naive div by ten
# result    : we see 'U' outputted onscreen
# @note     : the program ex5_inmul implements a way better div
# very naive algorithm that does division of 10 of the value in r0
# init
    nop
    mov r0, 78      # test value
    mov r1, r0
    mov r2, 0       # res
    mov r3, 10      # cst for jl purposes
dvision:
jlt r1, r3, divdone
    subi r1, r1, 10
    addi r2, r2, 1
    jmp dvision
divdone:
    add r2, r2, r0
    out r2          # we should get output 'U'
end:
    jmp end
