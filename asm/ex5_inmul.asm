# --- ex5_inmul.asm ---
# test      : mul, jlt, jne and various other functions
# result    : when trying to do 5 times 34, we get 170 (correct output)
# program that reads two users int input (mod 255) and prints their multiplication

# ### MANUAL ### #
# enter, character per character the first int
# press enter to confirm input
# enter then, character per character, the second int
# press enter to confirm input
# the result mod 255 should be then printed on screen
# ############## #

# init
    nop
    mov r0, 255         # cst for jeq purposes (keyboard input)
    mov r4, 13          # cst for jeq purposes (Enter input)
    mov r1, 0           # number stocker 1
    mov r5, 0           # number stocker 2
    mov r6, 0           # result
input:
    in r3               # char of user
    jeq r0, r3, input   # block until we get a char different than empty input
    nop                 # nop to clear buffer and avoid unwanted input
    nop
    nop
    nop
    out r3
    jne r3, r4, process
    jmp nextint         # goto second int input if user press [Enter] on keyboard
process:
    sub r3, r3, 30      # we assume that the user inputed valid char between '0' and '9'
    sub r3, r3, 18      # ascii -> int
    mov r2, r1          # muli cannot take the same arg twice
    muli r1, r2, 10 
    add r1, r1, r3
    jmp input
nextint:                # we start all over again, for second int
    in r3               # char of user
    jeq r0, r3, nextint # block until we get a char different than empty input
    nop                 # nop to clear buffer and avoid unwanted input
    nop
    nop
    nop
    out r3
    jne r3, r4, pagain  # goto print sequence if user press [Enter] on keyboard
    jmp final
pagain:
    sub r3, r3, 30      # we assume that the user inputed valid char between '0' and '9'
    sub r3, r3, 18      # ascii -> int
    mov r2, r5          # muli cannot take the same arg twice
    muli r5, r2, 10 
    add r5, r5, r3
    jmp nextint
final:
    mul r6, r5, r1      # final mult
    out r4              # \n
    nop                 # now that we have r6, we can reuse other registers
    mov r1, r6          # r1 = remainder 1
    mov r2, 0           # remainder 2
    mov r4, 0           # div
    mov r3, 10          # cst for jl purposes
dvision:
# since the comparaison are done on signed bits, 
# we need to not do the next comparaison if this number turns out to be greater than 128
jlt r1, r4, numbergreaterthan128 # if r1 < r4 
jlt r1, r3, div1done    #
numbergreaterthan128:
    subi r1, r1, 10
    addi r2, r2, 1
    jmp dvision
div1done:               # r1 has the least important decimal number
jlt r2, r3, div2done
    subi r2, r2, 10
    addi r4, r4, 1
    jmp div1done
div2done:               # r4 has the most important decimal number. 
                        # We don't need to go further since r6 < 255
    addi r4, r4, 30
    addi r4, r4, 18
    addi r2, r2, 30
    addi r2, r2, 18
    addi r1, r1, 30
    addi r1, r1, 18     # int --> ascii
    out r4
    out r2
    out r1              # we should have the decimal result of both input
end:
    jmp end
