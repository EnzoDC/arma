# --- ex5_input.asm ---
# test      : muli, jeq
# result    : when we input 35, the program outputs '#' (ascii code 35)
# @note     : for actually showing the result in decimal, see ex5_inmul.asm
# program that reads the user's int (mod 255) and prints it once user press Enter
# init
    nop
    mov r0, 255     # cst for jeq purposes (keyboard input)
    mov r4, 13      # cst for jeq purposes (Enter input)
    mov r1, 0       # number stocker
input:
    in r3           # char of user
# block until we get a char different than empty input
    jeq r0, r3, input
# nop to clear buffer and avoid unwanted input
    nop
    nop
    nop
    nop
# goto print sequence if user press [Enter] on keyboard
    jne r3, r4, process
    out r1 # prints result (ascii char)
end:
    jmp end
process:
# we assume that the user inputted valid char between '0' and '9'
    sub r3, r3, 30
    sub r3, r3, 18 # ascii -> int
    mov r2, r1  # muli cannot take the same arg twice
    muli r1, r2, 10 
    add r1, r1, r3
    jmp input
