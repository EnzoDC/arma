# ---ex5_jr.asm---
# test      : test jr
# result    : we see do see indeed the processor jumping to PC=10F
# @note     : jr doesn't work with labels
mov r1, 1
mov r2, 15
jr r1, r2       # we should land on pc 256 + 15 = 271 = 0x10F
