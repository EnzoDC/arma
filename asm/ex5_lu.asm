# ---ex5_lu.asm---
# test      : not, and, lsl, lsr, or
# result    : we see "BJ%m" outputted onscreen as expected
    nop
    mov r0, 33
    mov r1, 8 
    lsl r0, r0, 1   # (33 << 1) = 66 = 0b01000010
    out r0          # we shoud see 'B' on the screen
    or r0, r1, r0
    out r0          # we should see 'J'
    lsr r0, r0, 1
    mov r1, 63
    and r0, r0, r1
    out r0          # we should see '%'
    not r0, r0
    lsr r0, r0, 1
    out r0          # we should see 'm'
end:
    jmp end
