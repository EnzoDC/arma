# --- ex5_mul ---
# test      : mul
# @ensures  : prints L if the mul was done correctly, M otherwise
# result    : we see 'L' appears on screen, as expected
    nop
    mov r0, 2
    mov r1, 120
    mov r2, 0
    mov r3, 240 # wanted result
    mul r2, r0, r1
    jne r2, r3, wrong
# good
    mov r4, 76 
    out r4
    jmp end
wrong:
    mov r4, 77 
    out r4
    jmp end
end:
    jmp end
