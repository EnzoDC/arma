# --- ex5_muli.asm ---
# test      : muli
# result    : we see "!"  (ascii code 33, as expected)
    nop
    mov r0,  11
    muli r1, r0, 3
    out r1  # r1 = 33
end:
    jmp end
