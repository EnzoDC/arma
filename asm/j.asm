# --- j.asm ---
# test 		: the jump instruction
# result	: the program loops between PC 0, 1 and 2
start:
	ldi r1, 42
	addi r0, r1, 17	
	jmp start
