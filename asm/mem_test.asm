# --- mem.asm ---
# test      : test the ld and st functions
# result    : we see "0a" as expected
    nop
    ldi r1, 48
    ldi r0, 16
    mov (r0), r1
    mov r2, (r0)
    out r2 # we should see '0'
    mov r1, 97
    mov r0, 10
    st r1, r0, 4
    mov r0, 9
    ld r2, r0, 5
    out r2 # we should see 'a'
end:
    jmp end
