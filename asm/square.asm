# --- square.asm ---
# test      : jeq, in, out subi
# result    : for input '5', then 'q', we get
#              qqqqq
#              qqqqq
#              qqqqq
#              qqqqq
#              qqqqq
#             on screen.
#
# @require  : valid input n > 48 (expected char 0 to 9), char c
# @ensures  : output square of char c and of size n
# exemple   :
# n=0 c='A'
# 
#
# n=2 c='B'
# BB
# BB
# --------------------
# WARN : Keyboard buffer must be clean (255)
# --------------------
    nop
    ldi r0, 48              # ascii for 0
    ldi r1, -1              # check for empty input
    ldi r6, 13              # [Enter] ascii
# get usr input
input1:
    in r2                   # n
    jeq r1, r2, input1      # block until we get a char different than empty input
    nop
    nop
    nop
    nop                     # clear buffer with 4 nop
input2:
    in r3                   # char
    jeq r1, r3, input2      # block until we get a char different than empty input
    nop
    nop
    nop
    nop                     # clear buffer with 4 nop
# while loop
    addi r4, r2, 0          # int i = n; 
loop:
    jeq r0, r4, end
    addi r5, r2, 0          # int j = n;
    subloop:
        jeq r0 , r5, endsubloop
        out r3              # out char
        subi r5, r5, 1      # j-=1
        jmp subloop
    endsubloop:
        subi r4, r4, 1      # i-=1
        out r6
        jmp loop
end:
    jmp end
