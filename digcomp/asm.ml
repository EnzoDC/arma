(**
    Module providing types and functions to handle assembly code.
 *)


(**
    Type for the conditions in conditional jumps
 *)
type cond = LT | LE | EQ | NE


(**
    Type for the name of labels
 *)
type label = string



(**
    Type for one assembly instruction:

    {ul 
      {- rd = register where we will store the result of the operation}
      {- rs = first source register}
      {- rt = second source register}
      {- immX = immediate integer value on X bits}
      {- uimmX = immediate unsigned-integer value on X bits}
    }
 *)
 type instr =
        Nop
        | Ldi   of (int * int)   (** rd, imm8 *)
        | Add   of (int * int * int * bool) (** rd, rs, rt, sub? *)
        | Addi  of (int * int * int * bool) (** rd, rs, uimm5, sub? *)
        | Load  of (int * int * int) (** rd, rs, imm5 *)
        | Store of (int * int * int) (** rs, rd, imm5 *)
        | In    of int         (** rd *)
        | Out   of int         (** rs *)
        | CJmp  of (int * int * label * cond) (** rs, rt, addr, cond *)
        | Jmp   of label (** addr *)
        | Jr    of (int * int) (**rd, rs*)
        | Not   of (int * int) (** rd, rs *)
        | Lsr   of (int * int * int) (**rd, rs, uimm5*)
        | Or    of (int * int * int) (** rd, rs, rt*)
        | And   of (int * int * int) (** rd, rs, rt*)
        | Lsl   of (int * int * int) (**rd, rs, uimm5*)
        | Muli  of (int * int * int) (**rd, rs, uimm5*)
        | Mul of (int * int * int) (**rd, rs, rt*)
        | Add_if of (int * int * int) (**rd, rs, rt*)
    


(**
    dump_instr i pretty-prints instruction i to the standard output.  
    @param i  some instruction i
 *)
let dump_instr = fun i -> match i with
| Nop -> Printf.printf ".\n"
| Ldi (r,v) -> Printf.printf "r%d <- %d\n" r v
| Add (rd,rs,rt,b) ->
        let c = if b then '-' else '+' in
        Printf.printf "r%d <- r%d %c r%d\n" rd rs c rt
| Addi (rd,rs,v,b) ->
        let c = if b then '-' else '+' in
        Printf.printf "r%d <- r%d %c %d\n" rd rs c v
| Load (rd, rs, v) ->
        Printf.printf "r%d <- MEM[r%d+(%d)]\n" rd rs v
| Store (rs, rd, v) ->
        Printf.printf "MEM[r%d+(%d)] <- r%d\n" rs v rd
| In rd ->
        Printf.printf "r%d <- getchar()\n" rd
| Out rs ->
        Printf.printf "putchar(r%d)\n" rs
| CJmp (rd,rs,lbl,cd) ->
        let op = match cd with
                 | LT -> "<"
                 | LE -> "<="
                 | EQ -> "="
                 | NE -> "!="
        in
        Printf.printf "if r%d %s r%d: goto %s\n" rd op rs lbl
| Jmp lbl ->
        Printf.printf "goto %s\n" lbl
| Jr (rd, rs) ->
        Printf.printf "goto line 256*r%d + r%d" rd rs
| Not (rd, rs) ->
        Printf.printf "r%d <- not(r%d)\n" rd rs
| Lsr (rd, rs, v) ->
        Printf.printf "r%d <- r%d >> %d\n" rd rs v
| Lsl (rd, rs, v) ->
        Printf.printf "r%d <- r%d << %d\n" rd rs v
| Or  (rd, rs, rt) ->
        Printf.printf "r%d <- r%d || r%d\n" rd rs rt
| And (rd, rs, rt) ->
        Printf.printf "r%d <- r%d && r%d\n" rd rs rt
| Add_if (rd, rs, rt) ->
        Printf.printf "if (buffer=1) then r%d <- r%d + r%d\n" rd rs rt
| Muli (rd, rs, v) ->
        (*we actually should show what really happen..*)
        (*Should not happen*)
        Printf.printf "r%d <- r%d * %d\n" rd rs v
| Mul (rd, rs, rt) ->
        (*Same thing here*)
        (*Should not happen either*)
        Printf.printf "r%d <- r%d * r%d\n" rd rs rt

(*
 * requires: 0 < s < 30
 *)
let imm_of_int = fun s v ->
    let m = (1 lsl s) in      (* s-bit encoding = computing modulo m *)
    let v' = (v mod m) in     (* v' = v [m] and v' in [-m,m] *)
    let v'' = v'+m in         (* v'' = v [m] and v'' >= 0 *)
    v'' mod m


(* Type 1a: [xxx] [xx] [xxx]    [xxx] [xxx] 00
 *           cat  flags rd       rs    rt
 *)
let instr_to_bin_type1 = fun c f1 f2 r1 r2 r3 ->
    let hi = (c lsl 5) + (f1 lsl 4) + (f2 lsl 3) + r1 in
    let lo = (r2 lsl 5) + (r3 lsl 2) in
    (Printf.sprintf "%02x" hi, Printf.sprintf "%02x" lo)

(* Type 1b: [xxx] [xx] [xxx]    [xxx] [xxxxx]
 *           cat  flags rd       rs     imm5
 *)
let instr_to_bin_type1b = fun c f1 f2 r1 r2 v ->
    let hi = (c lsl 5) + (f1 lsl 4) + (f2 lsl 3) + r1 in
    let lo = (r2 lsl 5) + (imm_of_int 5 v) in
    (Printf.sprintf "%02x" hi, Printf.sprintf "%02x" lo)


(* Type 2: [xxx] [xx] [xxx]      [xxxxxxxx]
 *          cat  flags rd           imm8
 *)
let instr_to_bin_type2 = fun c f1 f2 r v ->
    let hi = (c lsl 5) + (f1 lsl 4) + (f2 lsl 3) + r in
    let lo = imm_of_int 8 v in
    (Printf.sprintf "%02x" hi, Printf.sprintf "%02x" lo)

(* Type 3: [xxx] [xxxxx           xxxxxxxx]
 *          cat             imm13
 *)
let instr_to_bin_type3 = fun c v ->
    let v' = imm_of_int 13 v in
    let hi = (c lsl 5) + (v' lsr 8) in
    let lo = v' mod 256 in
    (Printf.sprintf "%02x" hi, Printf.sprintf "%02x" lo)



(**
    instr_to_bin i nb assoc computes the encoding of instruction i.
    @param i instruction to encode
    @param caddr line number of instruction i
    @param assoc list that associates a label to its line number
    @return a pair of strings (s1,s2), where
    {ul
      {- s1 = hexadecimal code that can be loaded in the "high" SRAM}
      {- s2 = hexadecimal code that can be loaded in the "low" SRAM}
    }
*)
let instr_to_bin = fun i caddr assoc ->
    match i with
    | Nop ->
            instr_to_bin_type2 0b000 0 0 0 0
    | Ldi (r,v) ->
            instr_to_bin_type2 0b000 0 1 r v
    | Add (rd,rs,rt,b) ->
            let f1 = if b then 1 else 0 in
            instr_to_bin_type1 0b010 f1 1 rd rs rt
    | Addi (rd,rs,v,b) ->
            let f1 = if b then 1 else 0 in
            instr_to_bin_type1b 0b010 f1 0 rd rs v
    | Load (rd,rs,v) ->
            instr_to_bin_type1b 0b100 0 1 rd rs v
    | Store (rs,rd,v) ->
            instr_to_bin_type1b 0b100 0 0 rd rs v
    | In rd ->
            instr_to_bin_type2 0b100 1 1 rd 0
    | Out rs ->
            instr_to_bin_type2 0b100 1 0 rs 0
    | CJmp (rd,rs,lbl,cd) ->
            let (f1,f2) = match cd with
                          | EQ -> (0,0)
                          | LE -> (0,1)
                          | LT -> (1,0)
                          | NE -> (1,1)
            in
            let addr = List.assoc lbl assoc in
            let v = addr - caddr - 1 in
            if -16 <= v && v <= 15 then
                instr_to_bin_type1b 0b110 f1 f2 rd rs v
            else
                failwith ("Conditionnal jump: target ("
                          ^ lbl ^ ":" ^ string_of_int addr
                          ^ ") is too far from current address ("
                          ^ string_of_int caddr ^ ")")
    | Jmp lbl ->
            let addr = List.assoc lbl assoc in
            instr_to_bin_type3 0b111 addr
    | Jr (rd, rs) ->
            instr_to_bin_type1b 0b101 0 0 rd rs 0
    | Not (rd, rs) ->
            instr_to_bin_type1b 0b001 0 0 rd rs 0
    | Lsr (rd, rs, v) ->
            instr_to_bin_type1b 0b001 0 1 rd rs v
    | Lsl (rd, rs, v) ->
            instr_to_bin_type1b 0b000 1 1 rd rs v
    | Or  (rd, rs, rt) ->
            instr_to_bin_type1 0b001 1 0 rd rs rt
    | And (rd, rs, rt) ->
            instr_to_bin_type1 0b001 1 1 rd rs rt
    | Add_if (rd, rs, rt) ->
            instr_to_bin_type1 0b011 0 1 rd rs rt
    | Muli _->
            failwith "Error : match muli as a single expr instead of a macro"
    | Mul _->
            failwith "Error : match muli as a single expr instead of a macro"

(**
    macro_instr_to_instr i nb assoc returns a list of instr from one instr (for macro)
    This is better than macro_instr_to_bin because it ease the process parsing-wise
    @param i instruction to encode
    @return a list of instr
   @see "digcomp.ml" The compiler knows when a macro is passed by checking specific i values
*)
let macro_instr_to_instr = fun i ->
        match i with
        | Muli (rd, rs, v) ->
                (*  i have decided that the multiplication will be done by left-shifting values
                since : 
                        -implementing the leftshift was very easy using the already existing
                         rightshift components of the ALU
                        -multiplication can be done in at worst 10 cycles, and at best 5 cycles
                         with shifting. The result is modulo 255 right
                for instance, for n times 10, we instead do :
                        n*8 + n*2 = (n<<3) + (n<<2)
                        this multiplication takes 3 cycles (2 leftshifts and 1 addition)
                
                This has however has some (unforseen) flaws :
                        -we cannot use less than two register, thus we cannot use muli rd, rd, 5 for instance.
                We add an exception in the lexer to prevent this
                        -it is destructive (meaning that rs is not unchanged).*)
                (*we first put zero in rd*)
                let first_op = Ldi (rd, 0) (*ldi rd, 0 ; rd<-0*) in
                        let aux_add_if poweroftwo li=
                                if (v land poweroftwo)=poweroftwo then
                                        (* add rd, rd, rs ; rd<-rd+rs*)
                                        (Add (rd,rd,rs,false))::li
                                else
                                        li
                        in

                        let aux power li =
                                (*Lsl rs, 1*)
                                let next = (Lsl (rs, rs, 1)) in
                                next::(aux_add_if power li)
                        in

                let out_final =
                        let op1 = (aux 16 []) in
                        let op2 = (aux 8 op1) in
                        let op3 = (aux 4 op2) in
                        let op4 = (aux 2 op3) in
                        let op5 = aux_add_if 1 op4 in
                        first_op::op5
                        (*v is leq 31, thus we cant add more*)
                        (*each "aux n" calls do :
                        -byteshift left once rs
                        -check if v mod 2**n is zero. if so, adds rs value into rd. Else do nothing.
                        this is basically doing the equivalent of :
                        rd += (v % 2 = 0)*(rs<<1) 
                        rd += (v % 4 = 0)*(rs<<2)
                        ...
                        *)
                in
                List.rev out_final (* i messed up somewhere...*)

        | Mul (rd, rs, rt) ->
                (*Following the idea not to expand the ALU, this multiplication 
                        of two register values uses right shift and left shift to do the
                        rd <-- rs * rt multiplication
                This is done in a constant time of 16 cycle, and by modifying a bit the processor with a buffer

                This however, also have some flaws : the value of rs and rt is destroyed through the process, and we need
                to have rs != rt, rd != rs, rt != rd 

                To do this operation though, we first
                rd <-- 0, then look at the least important bit of rt through a buffer,
                which will then carry to the next operation
                011 01 "rd" "rs" "rt"
                the flag "do_mul" is on when control bits=01101, and with an and gate in the processor with this flag and the
                bit stored in the buffer, we triggers the write mode of the register thus stocking rd+rs
                in rd.

                We then shift rs left once, and rt right once, and we repeat all of this four times.
                        *)
                (*we first set rd to zero*)
                let first_op = Ldi (rd, 0) in (*ldi rd, 0 ; rd<-0*)
                (*we then read rt*)
                let second_op = Lsr (rt, rt, 0) in (*rt >> 0*)
                (*we then conditionnaly put rt in rd*)
                let add_if = Add_if (rd, rd, rs) in
                let rec aux acc =
                        if (acc=0) then
                                []
                        else
                                
                                
                                let add_if = (Add_if (rd, rd, rs)) in (*011 01 rd rd rs *) (*which is basically a conditionnal add*)
                                let shiftl = (Lsl (rs, rs, 1)) in (*rs << 1*)
                                let shiftr = (Lsr (rt, rt, 1)) in (*rt >> 1*)
                                shiftl::(shiftr::(add_if::(aux (acc-1))))
                in
                let out_final = first_op::(second_op::(add_if::(aux 7))) in
                List.rev out_final (*i messed up somewhere urgh*)
        | _ ->
                failwith "Unexpected matching of a non-macro instr"










(*
    macro_instr_to_bin i nb assoc computes the encoding of instruction i into multiple instructions.
    @param i instruction to encode
    @param caddr line number of instruction i
    @param assoc list that associates a label to its line number
    @return a tuple composed of an int n and a list of pair of strings (s1,s2), wheres
    {ul
      {- n = the length of the list (needed for appropriate labels and jumps)}
      {- s1 = hexadecimal code that can be loaded in the "high" SRAM}
      {- s2 = hexadecimal code that can be loaded in the "low" SRAM}
    }
   see "digcomp.ml" The compiler knows when a macro is passed by checking specific i values
   IMPORTANT NOTE : DEPRECATED
let macro_instr_to_bin = fun i caddr assoc ->
        match i with
        | Muli (rd, rs, v) ->
        
            (*  i have decided that the multiplication will be done by left-shifting values
                since : 
                        -implementing the leftshift was very easy using the already existing
                         rightshift components of the ALU
                        -multiplication can be done in at worst 10 cycles, and at best 5 cycles
                         with shifting. The result is modulo 255 right
                for instance, for n times 10, we instead do :
                        n*8 + n*2 = (n<<3) + (n<<2)
                        this multiplication takes 3 cycles (2 leftshifts and 1 addition)
                
                This has however has some (unforseen) flaws :
                        -we cannot use less than two register, thus we cannot use muli rd, rd, 5 for instance.
                We add an exception in the lexer to prevent this
                        -it is destructive (meaning that rs is not unchanged).
           *)
           (*we first put zero in rd*)
           let first_op = instr_to_bin_type2 0b000 0 1 rd 0 (*ldi rd, 0 ; rd<-0*) 
           in
                let aux_add_if poweroftwo li=
                        if (v land poweroftwo)=poweroftwo then
                                (* add rd, rd, rs ; rd<-rd+rs*)
                                (*let _ = Printf.printf "%s\n" (string_of_int rd) in*)
                                (instr_to_bin_type1 0b010 0 1 rd rd rs)::li
                        else
                                li
                in

                let aux power li =
                        (*lsf rs, 1*)
                        let next = (instr_to_bin_type1b 0b000 1 1 rs rs 1) in 
                        next::(aux_add_if power li)
                in

        let out_final =
                let op1 = (aux 16 []) in
                let op2 = (aux 8 op1) in
                let op3 = (aux 4 op2) in
                let op4 = (aux 2 op3) in
                let op5 = aux_add_if 1 op4 in
                first_op::op5
                (*v is leq 31, thus we cant add more*)
                (*each "aux n" calls do :
                   -byteshift left once rs
                   -check if v mod n is zero. if so, adds rs value into rd. Else do nothing.
                this is basically doing the equivalent of :
                   rd += (v % 2 = 0)*(rs<<1) 
                   rd += (v % 4 = 0)*(rs<<2)
                   ...
                *)
        in
        let n=List.length out_final in (*I know this is not the best way to count the length of the list, but refactoring comes later*)
        (n,out_final)
        | Mul (rd, rs, rt) ->
                (*Following the idea not to expand the ALU, this multiplication 
                   of two register values uses right shift and left shift to do the
                   rd <-- rs * rt multiplication
                This is done in a constant time of 16 cycle, and by modifying a bit the processor with a buffer

                This however, also have some flaws : the value of rs and rt is destroyed through the process, and we need
                to have rs != rt, rd != rs, rt != rd 

                To do this operation though, we first
                rd <-- 0, then look at the least important bit of rt through a buffer,
                which will then carry to the next operation
                011 01 "rd" "rs" "rt"
                the flag "do_mul" is on when control bits=01101, and with an and gate in the processor with this flag and the
                bit stored in the buffer, we triggers the write mode of the register thus stocking rd+rs
                in rd.

                We then shift rs left once, and rt right once, and we repeat all of this four times.
                   *)
                (*we first set rd to zero*)
                let first_op = instr_to_bin_type2 0b000 0 1 rd 0 in (*ldi rd, 0 ; rd<-0*)
                (*we then read rt*)
                let second_op = instr_to_bin_type1b 0b001 0 1 rt rt 0 in (*rt >> 1*)
                (*we then conditionnaly put rt in rd*)
                let add_if = instr_to_bin_type1 0b011 0 1 rd rd rs in
                let rec aux acc =
                        if (acc=0) then
                                []
                        else
                                
                                
                                let add_if = (instr_to_bin_type1 0b011 0 1 rd rd rs) in (*011 01 rd rd rs *) (*which is basically a conditionnal add*)
                                let shiftl = (instr_to_bin_type1b 0b000 1 1 rs rs 1) in (*rs << 1*)
                                let shiftr = (instr_to_bin_type1b 0b001 0 1 rt rt 1) in (*rt >> 1*)
                                shiftl::(shiftr::(add_if::(aux (acc-1))))
                in
                let out_final = first_op::(second_op::(add_if::(aux 4))) in
                let n = List.length out_final in
                (n,out_final)


        | _ -> failwith "Unexpected matching of a non-macro instr"
*)