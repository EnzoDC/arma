(**
    Module providing types and functions to handle assembly code.
 *)


(**
    Type for the conditions in conditional jumps
 *)
type cond =
    LT (** lower than *)
  | LE (** lower than or equal to *)
  | EQ (** equal to *) 
  | NE (** not equal to *)


(**
    Type for the name of labels
 *)
type label = string


(**
    Type for one assembly instruction:
    
    {ul 
      {- rd = register where we will store the result of the operation}
      {- rs = first source register}
      {- rt = second source register}
      {- immX = immediate integer value on X bits}
      {- uimmX = immediate unsigned-integer value on X bits}
    }
 *)
type instr =
      Nop
    | Ldi   of (int * int)   (** rd, imm8 *)
    | Add   of (int * int * int * bool) (** rd, rs, rt, sub? *)
    | Addi  of (int * int * int * bool) (** rd, rs, uimm5, sub? *)
    | Load  of (int * int * int) (** rd, rs, imm5 *)
    | Store of (int * int * int) (** rs, rd, imm5 *)
    | In    of int         (** rd *)
    | Out   of int         (** rs *)
    | CJmp  of (int * int * label * cond) (** rs, rt, addr, cond *)
    | Jmp   of label (** addr *)
    | Jr    of (int * int) (**rd, rs*)
    | Not   of (int * int) (** rd, rs *)
    | Lsr   of (int * int * int) (**rd, rs, uimm5*)
    | Or    of (int * int * int) (** rd, rs, rt*)
    | And   of (int * int * int) (** rd, rs, rt*)
    | Lsl   of (int * int * int) (**rd, rs, uimm5*)
    | Muli  of (int * int * int) (**rd, rs, uimm5*)
    | Mul   of (int * int * int) (**rd, rs, rt*)
    | Add_if of (int * int * int) (**rd, rs, rt*)


(**
    dump_instr i pretty-prints instruction i to the standard output.  
    @param i  some instruction i
 *)
val dump_instr : instr -> unit


(**
    instr_to_bin i caddr assoc computes the encoding of instruction i.
    @param i instruction to encode
    @param caddr line number of instruction i
    @param assoc list that associates a label to its line number
    @return a pair of strings (s1,s2), where
    {ul
      {- s1 = hexadecimal code to load in the "high" SRAM}
      {- s2 = hexadecimal code to load in the "low" SRAM}
    }
*)
val instr_to_bin : instr -> int -> (label * int) list -> string * string

(**
    macro_instr_to_instr i returns a list of instr from one instr (for macro)
    This is better than macro_instr_to_bin because it ease the process parsing-wise
    @param i instruction to encode
    @return a list of instr
   @see "digcomp.ml" The compiler knows when a macro is passed by checking specific i values
*)
val macro_instr_to_instr : instr -> (instr list)

(*Deprecated
(*
    macro_instr_to_bin i nb assoc computes the encoding of instruction i into multiple instructions.
    @param i instruction to encode
    @param caddr line number of instruction i
    @param assoc list that associates a label to its line number
    @return a tuple composed of an int n and a list of pair of strings (s1,s2), wheres
    {ul
      {- n = the length of the list (needed for appropriate labels and jumps)}
      {- s1 = hexadecimal code to load in the "high" SRAM}
      {- s2 = hexadecimal code to load in the "low" SRAM}
    }
   @see "digcomp.ml" The compiler knows when a macro is passed by checking specific i values
*)
val macro_instr_to_bin : instr -> int -> (label * int) list -> (int * (string * string) list)
*)