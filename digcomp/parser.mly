%{
    open Asm
%}


%token <int> INT
%token NOP MOV ADD ADDI SUB SUBI JMP LD ST IN OUT JLE JLT JEQ JNE NOT OR LSR AND LSL MULI MUL ADD_IF JR
%token COMA COLON LPAR RPAR
%token <int> REG
%token <string> LABEL
%token EOL

%start main     
%type <(Asm.label * Asm.instr option)> main
%type <Asm.cond> cjump

%%

main:
    LABEL COLON                  { ($1, None) }
  | EOL                          { ("", None) }
  | code EOL                     { ("", Some $1) }
  | code                         { ("", Some $1) }
;

code:
  | NOP                           { Nop }
  | MOV REG COMA INT              { Ldi ($2,$4) }
  | MOV REG COMA REG              { Addi ($2,$4,0,false)  }
  | ADD REG COMA REG COMA REG     { Add  ($2,$4,$6,false) }
  | ADDI REG COMA REG COMA INT    { assert (0<=$6 && $6<32); Addi ($2,$4,$6,false) }
  | ADD REG COMA REG COMA INT     { assert (0<=$6 && $6<32); Addi ($2,$4,$6,false) }
  | ADD REG COMA INT COMA REG     { assert (0<=$4 && $4<32); Addi ($2,$6,$4,false) }
  | SUB REG COMA REG COMA REG     { Add  ($2,$4,$6,true)  }
  | SUB REG COMA REG COMA INT     { assert (0<=$6 && $6<32); Addi ($2,$4,$6,true)  }
  | SUBI REG COMA REG COMA INT    { assert (0<=$6 && $6<32); Addi ($2,$4,$6,true)  }
  | LD REG COMA REG COMA INT      { assert (0<=$6 && $6<32); Load ($2, $4, $6)  }
  | MOV REG COMA LPAR REG RPAR    { Load ($2, $5, 0)  }
  | ST REG COMA REG COMA INT      { assert (0<=$6 && $6<32); Store ($4, $2, $6) }
  | MOV LPAR REG RPAR COMA REG    { Store ($3, $6, 0) }
  | IN REG                        { In $2  }
  | OUT REG                       { Out $2 }
  | cjump REG COMA REG COMA LABEL { CJmp ($2,$4,$6,$1) }
  | JMP LABEL                     { Jmp $2 }
  | JR REG COMA REG               { Jr ($2, $4) }
  | AND REG COMA REG COMA REG     { And ($2,$4,$6) } 
  | NOT REG COMA REG              { Not ($2,$4) }
  | LSR REG COMA REG COMA INT     { assert (0<=$6 && $6<32); Lsr ($2,$4,$6)      }
  | OR REG COMA REG COMA REG      { Or ($2,$4,$6)}
  | LSL REG COMA REG COMA INT     { assert (0<=$6 && $6<32); Lsl ($2,$4,$6)      }
  | MULI REG COMA REG COMA INT    { assert (0<=$6 && $6<32); assert ($2 <> $4) ; Muli ($2,$4,$6)      }
  | MUL REG COMA REG COMA REG     { assert ($2 <> $6); assert ($2 <> $4) ; assert ($6 <> $4) ; Mul ($2,$4,$6)      }
  | ADD_IF REG COMA REG COMA REG  { Add_if ($2, $4, $6)}
;

cjump:
  | JLE  { LE }
  | JLT  { LT }
  | JEQ  { EQ }
  | JNE  { NE }
;
